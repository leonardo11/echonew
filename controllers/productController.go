package controllers

import (
	"net/http"
	"strconv"

	"gitlab.com/leonardo11/echonew/models"

	"github.com/labstack/echo"
)

func FetchAllProduct(c echo.Context) error {
	result, err := models.FetchAllProduct()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)
}

func CreateProduct(c echo.Context) error {
	name := c.FormValue("name")
	price := c.FormValue("price")
	quantity := c.FormValue("quantity")
	status := c.FormValue("status")

	result, err := models.CreateProduct(name, price, quantity, status)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, result)
}

func UpdateProduct(c echo.Context) error {
	id := c.FormValue("id")
	name := c.FormValue("name")
	price := c.FormValue("price")
	quantity := c.FormValue("quantity")
	status := c.FormValue("status")

	conv_id, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	result, err := models.UpdateProduct(conv_id, name, price, quantity, status)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, result)
}

func DeleteProduct(c echo.Context) error {
	id := c.FormValue("id")

	conv_id, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	result, err := models.DeleteProduct(conv_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, result)
}
