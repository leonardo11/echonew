package main

import (
	"gitlab.com/leonardo11/echonew/db"
	"gitlab.com/leonardo11/echonew/routes"
)

func main() {

	db.Init()

	e := routes.Init()

	e.Logger.Fatal(e.Start(":1323"))
}
