package models

import (
	"net/http"

	"gitlab.com/leonardo11/echonew/db"
)

type Product struct {
	Id       int64   `json:"id"`
	Name     string  `json:"name"`
	Price    float32 `json:"price"`
	Quantity int     `json:"quantity"`
	Status   bool    `json:"status"`
}

func FetchAllProduct() (response, error) {
	var obj Product
	var arrobj []Product
	var res response

	con := db.CreateCon()

	sqlStatement := "SELECT * FROM product"

	rows, err := con.Query(sqlStatement)
	defer rows.Close()

	if err != nil {
		return res, err
	}

	for rows.Next() {
		err = rows.Scan(&obj.Id, &obj.Name, &obj.Price, &obj.Quantity, &obj.Status)
		if err != nil {
			return res, err
		}

		arrobj = append(arrobj, obj)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = arrobj

	return res, nil
}

func CreateProduct(name string, price string, quantity string, status string) (response, error) {
	var res response

	con := db.CreateCon()

	sqlStatement := "INSERT product (name, price, quantity, status) VALUES (?, ?, ?, ?)"
	stmt, err := con.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(name, price, quantity, status)
	if err != nil {
		return res, err
	}

	lastInsertedId, err := result.LastInsertId()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"last_inserted_id": lastInsertedId,
	}

	return res, nil
}

func UpdateProduct(conv_id int, name string, price string, quantity string, status string) (response, error) {
	var res response

	conn := db.CreateCon()

	sqlStatement := "UPDATE product SET name = ?, price = ?, quantity = ?, status = ? WHERE id = ?"

	stmt, err := conn.Prepare(sqlStatement)
	if err != nil {
		return res, nil
	}

	result, err := stmt.Exec(name, price, quantity, status, conv_id)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}

func DeleteProduct(conv_id int) (response, error) {
	var res response

	conn := db.CreateCon()

	sqlStatement := "DELETE FROM product WHERE id = ?"

	stmt, err := conn.Prepare(sqlStatement)
	if err != nil {
		return res, err
	}

	result, err := stmt.Exec(conv_id)
	if err != nil {
		return res, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return res, err
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = map[string]int64{
		"rows_affected": rowsAffected,
	}

	return res, nil
}
