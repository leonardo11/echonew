package routes

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/leonardo11/echonew/controllers"
)

func Init() *echo.Echo {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.GET("/products", controllers.FetchAllProduct)
	e.POST("/products", controllers.CreateProduct)
	e.PUT("/products", controllers.UpdateProduct)
	e.DELETE("/products", controllers.DeleteProduct)

	return e
}
